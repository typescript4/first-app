class Vehicle {
  protected brandName: string;
  private model: string;
  private color: string;
  private price: number;

  constructor(brandName: string, model: string, color: string, price: number) {
    this.brandName = brandName;
    this.model = model;
    this.color = color;
    this.price = price;
  }

  get getPrice() {
    return this.price;
  }

  set setPrice(price: number) {
    this.price = price;
  }

  drive() {
    console.log(
      `conduciendo un ${this.brandName} modelo: ${this.model} color: ${this.color}`
    );
  }

  static priceToCurrency(value: number, typeOfCurrency: string) {
    let result = "";
    switch (typeOfCurrency) {
      case "USD":
        result = "US " + value;
        break;
      case "PESO":
        result = "$ " + value;
        break;
    }

    return result;
  }
}

const vehicle = new Vehicle("Toyota", "2", "rojo", 2000);
console.log(vehicle);
/* vehicle.color = "azul";
console.log(vehicle);     (Al añadir *private* a los atributos de la clase, se restringe su modificación fuera de ella)*/
vehicle.drive();
console.log(Vehicle.priceToCurrency(vehicle.getPrice, "USD"));

// HERENCIA
console.log("\n----------- HERENCIA -----------");
class Car extends Vehicle {
  drive(): void {
    console.log(`Estoy conduciendo un auto ${this.brandName}`);
  }
}
const car = new Car("M&M", "71", "plata", 4000);
console.log(car);
car.drive();
car.setPrice = 5000;
console.log(car.getPrice);

// OTRA FORMA DE CREAR UNA CLASE Y SUS ATRIBUTOS
console.log(
  "\n----------- OTRA FORMA DE CREAR UNA CLASE Y SUS ATRIBUTOS -----------"
);
abstract class Vehicle2 {
  constructor(
    protected readonly brandName: string,
    private readonly model: string,
    private readonly color: string
  ) {}

  drive() {
    // this.brandName = "BMW";  (Para restringir la modificación de los atributos a solo el constructor, podemos añadir *readonly* a los atributos)
    console.log(
      `conduciendo un ${this.brandName} modelo: ${this.model} color: ${this.color}`
    );
  }

  abstract drive2(): void;
}

class Car2 extends Vehicle2 {
  drive(): void {
    console.log(`Estoy conduciendo un auto ${this.brandName}`);
  }

  drive2(): void {
    console.log("Estoy implementando metodo abstracto en Car2");
  }
}
class Airplane extends Vehicle2 {
  drive2(): void {
    console.log("Estoy implementando metodo abstracto en Airplane");
  }
}

const vehicle2 = new Car2("Ferrari", "7", "Negro");
console.log(vehicle2);
vehicle2.drive2();

const airplane = new Airplane("Shark", "99", "Camuflaje");
console.log(airplane);
airplane.drive2();

// PROBANDO ALCANCE DE THIS
console.log("\n------------------ PROBANDO ALCANCE DE THIS ------------------");
let motorcycle = {
  //   brandName: "Yamaha",
  drive: vehicle.drive
};

motorcycle.drive(); /* (Al crear un nuevo objeto simple, el *this.brandName* tratará de tomar el valor brandName del objeto
                    que lo esta usando, en el caso de *motorcycle* no lo tiene por lo cual arrojará *undefined*) */

// INTERFACES CON CLASES
console.log("\n------------------ INTERFACES CON CLASES ------------------");

interface Billable {
  currentBillable(): string;
}

interface Flayable {
  altitude(): string;
}

class Car3 extends Vehicle2 implements Billable {
  drive2(): void {
    console.log("Estoy implementando metodo abstracto en Car3");
  }

  currentBillable(): string {
    return "La factura actual es de 200";
  }
}

class Airplane2 extends Vehicle2 implements Billable, Flayable {
  drive2(): void {
    console.log("Estoy implementando metodo abstracto en Airplane2");
  }

  currentBillable(): string {
    return "La factura actual es de 200";
  }

  altitude(): string {
    return "20000 pies";
  }
}

const vehicule3: Flayable = new Airplane2("Mitsubishi", "33", "Oro");
console.log(vehicule3);
