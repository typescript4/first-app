function sumar(valor: number, valor2: number): number {
  return valor + valor2;
}

function imprimir() {
  console.log(sumar(5, 9));
  //   return null;
  //   return undefined;
}

imprimir();

// ARROW FUNCTION
const suma = (valor1: number, valor2: number): number => {
  return valor1 + valor2;
};

console.log(suma(5, 3));

// FUNCION ANONIMA
const dividir = function(valor1: number, valor2: number) {
  return valor1 / valor2;
};

// ERROR FUNCTION
const throwError = (message: string) => {
  if (message) {
    throw new Error(message);
  }
  return "no hay error";
};

// throwError("hola mundo");

//FUNCTION COMO TIPO
let sumFunction: (valor1: number, valor2: number) => number;
sumFunction = suma;
/* sumFunction = imprimir;  (SIENDO FUNCTION EL TIPO DE DATO, SE PUEDE 
                            ESPECIFICAR CUALQUIER FUNCTION, PERO PUEDE QUE NO SEA LO QUE QUERAMOS) */
// sum = 'hola mundo';  (NO SE LE PUEDE ASIGNAR ALGO QUE NO SEA FUNCTION)

console.log(sumFunction(12, 22));

//FUNCIONES CALLBACK
function imprimir2(a: number, b: number, mostrar: (value: number) => void) {
  let sum = a + b;
  mostrar(sum);
}
imprimir2(3, 7, value => console.log(value));
