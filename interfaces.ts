enum Role {
  Doctor,
  Anestesista,
  Asistente,
  Administrativo
}

interface Staff {
  name: string;
  email: string;
  role: Role;
}

const medico = {
  name: "Richard",
  email: "richard@example.com",
  role: Role.Doctor,
  total: 25,
  currentBill(){
    return `Valor actual de la factura es de ${this.total}`;
  }
};

const printStaff = (staff: Staff) => {
  console.log(staff);
};

printStaff(medico);

interface Billable{
  total: number;
  currentBill(): string;
}

const printCurrentBill = (bill: Billable) =>{
  console.log(bill.currentBill());
};

printCurrentBill(medico);