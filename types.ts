// ----------------------------------- PRIMITIVOS ---------------------------------
// let vehiculo: string;
let vehiculo = "automovil";
let cantidad: number;
let esAutomovil: boolean;

// ----------------------------------- NO PRIMITIVOS ---------------------------------
// OBJETOS ------------------
console.log("------------- OBJETOS -------------");
// La declaración de tipado es opcional, peros si se especifica se debe declarar cada uno de los atributos.
/* let persona: {
    nombre: string,
    edad: number,
    direccion: {
        calle: string,
        comuna: string
    }
} = {
  nombre: 'Juan',
  edad: 34,
  direccion: {
      calle: 'Los Olivos',
      comuna: 'El Ultimo Aliento'
  }
}; */
let persona = {
  nombre: "Juan",
  edad: 34,
  direccion: {
    calle: "Los Olivos",
    comuna: "El Ultimo Aliento"
  },
  cursos: ["Matemática", "Literatura", "Física"]
};
console.log(persona);

// ARRAYS ------------------
console.log("------------- ARRAYS -------------");
let hobbies: string[] = ["Natación", "Jugar", "Escuchar música"];
console.log(hobbies);

let array: any[] = ["Natación", "Jugar", "Escuchar música", 13];
console.log(array);

for (const hobbie of hobbies) {
  console.log(hobbie.toUpperCase());
}

array[3] = "patinar";
console.log(array);

// TUPLAS ------------------
console.log("------------- TUPLAS -------------");
type vehiculoType = [string, number, string]; //Se declara un type para la instancia de otras variables
let automovil: vehiculoType = ["mazda", 2020, "rojo"];
let motocicleta: vehiculoType = ["yamaha", 2021, "azul"];
// automovil[1] = 'lavado';   (NO SE PERMITE)
console.log(automovil);

// ENUMS ------------------
console.log("------------- ENUMS -------------");
enum roles {
  estudiante,
  profesor
}

let persona2 = {
  nombre: "Juan",
  edad: 34,
  direccion: {
    calle: "Los Olivos",
    comuna: "El Ultimo Aliento"
  },
  cursos: ["Matemática", "Literatura", "Física"],
  rol: roles.estudiante
};

console.log(persona2);

// ANY ------------------
console.log("------------- ANY -------------");
let x: any[] = ["Ladrillo", 12, "Oceano"];
console.log(x);

// UNION ------------------
console.log("------------- UNION -------------");
type stringOrNumber = string | number;
function padLeft(value: string, padding: stringOrNumber) {
  if (typeof padding === "number") {
    return Array(padding + 1).join(" ") + value;
  }
  if (typeof padding === "string") {
    return padding + value;
  }
  throw new Error(`Espera unstring pero recibí '${padding}'.`);
}
console.log(padLeft('Hola Mundo', 10));

// LITERAL ------------------
console.log("------------- LITERAL -------------");
let vahiculo2: 'cuatrimoto';
// vahiculo2 = 'lancha';  (NO SE PERMITE)
console.log(vahiculo2);